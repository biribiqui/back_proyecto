const fs = require("fs");

function writeUserDataToFile(data)
{
  console.log("writeUserDataToFile");

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(error){
      if (error){
        console.log("error al escribir");
      }
      else {
        console.log("escritura correcta");
      }
    }
  )
}

module.exports.writeUserDataToFile = writeUserDataToFile;
