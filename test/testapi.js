const mocha = require("mocha");
const chai = require("chai");
const chaihttp = require("chai-http");

chai.use(chaihttp);

var should = chai.should();

describe("First test",
  function(){
    it("Test that duckduckgo works", function(done){
      chai.request("http://www.duckduckgo.com")
          .get("/")
          .end(
            function(err, res){
              console.log("Request finished");
              // console.log(res);
              // console.log(err);
              done();
            }
          )
    })
  })

describe("Test de API de usuarios",
  function(){
    it("Test that user api says hello", function(done){
      chai.request("http://localhost:3000")
          .get("/apitechu/v1/hello")
          .end(
            function(err, res){
              console.log("Request finished");
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde API Techu");
              done();
            }
          )
    })
  })

describe("Test de API de usuarios",
  function(){
    it("Test that user api return user list", function(done){
      chai.request("http://localhost:3000")
          .get("/apitechu/v1/users")
          .end(
            function(err, res){
              console.log("Request finished");
              res.should.have.status(200);
              res.body.usuarios.should.be.a("array");

              for (usuario of res.body.usuarios){
                usuario.should.have.property("id");
                usuario.should.have.property("email");
                usuario.should.have.property("password");                
              }
              done();
            }
          )
    })
  })
