require("dotenv").config();

const express = require("express");
const jwt = require("jsonwebtoken");
const app = express();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "content-type, authorization");

 next();
}

var jwtValidate = function (req, res, next) {

  var tokenjwt = req.headers.authorization;

  if (req.headers["access-control-request-headers"] === "authorization,content-type" ||
      req.headers["access-control-request-headers"] === "authorization" ||
      req.headers["access-control-request-headers"] === "content-type"){

      next();
  }else {

    if (!tokenjwt){

      res.status(401).send({"msg":"ERROR: Es necesario el token de autenticación"});
    }else {

      tokenjwt = tokenjwt.replace("Bearer ", "");

      jwt.verify(tokenjwt, "Secret Password", function(err) {
        if (err){

          res.status(401).send({"msg":"ERROR: Token inválido"});
        }else {

          next();
        }
      })
    }
  }
}

app.use(express.json());
app.use(enableCORS);

app.use("/apitechu/secure", jwtValidate);

const userController = require("./controllers/userController");
const authController = require("./controllers/authController");
const accountController = require("./controllers/accountController");
const movementController = require("./controllers/movementController");
const loansController = require("./controllers/loansController");

const port = process.env.PORT || 3000;

app.listen(port);

console.log("API escuchando en el puerto tururu " + port);

app.post("/apitechu/users/login/", authController.logonUser);

app.post("/apitechu/secure/users/logout/:id", authController.logoutUser);

app.post("/apitechu/users/alta", userController.createUser);

app.post("/apitechu/secure/users/baja/:id", userController.deleteUser);

app.post("/apitechu/secure/users/modifica/:id", userController.updateUser);

app.get("/apitechu/secure/accounts/:id", accountController.getAccountsById);

app.post("/apitechu/secure/accounts/iban", accountController.getAccountsByIban);

app.post("/apitechu/secure/accounts/alta", accountController.createAccount);

app.post("/apitechu/secure/accounts/update/:id", accountController.updateAccount);

app.get("/apitechu/secure/moves/:idfolio", movementController.getMovesById);

app.post("/apitechu/secure/moves/alta", movementController.createMoves);

app.post("/apicuadro/loans/cuadro/:apiKey", loansController.calculoCuadro);
