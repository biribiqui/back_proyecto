# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de capeta local a apitechu
ADD . /apitechu

# Instalación de las dependencias
RUN npm install --only=prod

# Puerto de trabajo
EXPOSE 3000

# Comando de inicialización
CMD ["node", "server.js"]
