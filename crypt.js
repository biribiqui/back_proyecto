const crypt = require("bcrypt");

function hash(data){
  console.log("Hashing data");

  return crypt.hashSync(data, 10);
}

function compareSync(data1, data2){

  return crypt.compareSync(data1, data2);
}

module.exports.hash = hash;
module.exports.compareSync = compareSync;
